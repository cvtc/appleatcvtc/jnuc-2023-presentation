from os import environ

from jps_api_wrapper.classic import Classic

JPS_URL = "https://example.jamfcloud.com"
JPS_USERNAME = environ.get("JPS_USERNAME")
JPS_PASSWORD = environ.get("JPS_PASSWORD")

with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic:
    mobile_devices = classic.get_mobile_devices()["mobile_devices"]

    for mobile_device in mobile_devices:
        device_id = mobile_device["id"]
        device_name = (
            mobile_device["username"]
            + "-"
            + mobile_device["serial_number"]
        )

        classic.create_mobile_device_command(
            "DeviceName", 
            [device_id], 
            device_name
        )
        print(f"Updated {device_id} name to {device_name}.")

